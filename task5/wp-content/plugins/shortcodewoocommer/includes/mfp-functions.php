<?
/*
 *Add my new menu to the Admin Control Pannel
*/
// ============================ them js css=========================================
?>

<?

function style_show()
{

	wp_enqueue_style('main_Css',plugins_url('main.css',__FILE__));
	 wp_enqueue_script('jqueryjs',plugins_url('jquery.min.js',__FILE__));// error no add produce

	 wp_enqueue_script('main_js',plugins_url('main.js',__FILE__));
}
add_action('admin_enqueue_scripts','style_show');
// scrip css cua slide
   function shortcodewoo_cript() {
   	wp_enqueue_style( 'wp-analytify-style', plugins_url('../assets/css/docs.theme.min.css', __FILE__));
   	wp_enqueue_style( 'wp-analytify', plugins_url('../assets/owlcarousel/assets/owl.carousel.min.css', __FILE__));

   	wp_enqueue_script( 'custom-script1', plugins_url( '../assets/vendors/jquery.min.js', __FILE__ ) );
   	wp_enqueue_script( 'custom-script2', plugins_url( '../assets/owlcarousel/owl.carousel.js', __FILE__ ) );

   	wp_register_style( 'Font_Awesome', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css' );
   	wp_enqueue_style('Font_Awesome');
   	wp_register_script( 'jQuery', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js', null, null, true );
   	wp_enqueue_script('jQuery');

   }
   add_action( 'wp_enqueue_scripts', 'shortcodewoo_cript' );


// ======================== them moi register_setting ==============================
function register_mysetting(){
	register_setting('short-code-woo','displayshow');
	register_setting('short-code-woo','fieldslistcategory');
	register_setting('short-code-woo','fieldsascdsc');
	register_setting('short-code-woo','showlist');
}
// ======================== them moi menu plugin ==============================
function addmenuwhortcode_woo()
{
	add_menu_page(
		'Shortcode woo', // tieu de menu
		'shortcode wooplugin', // text hien thi trong  menu
		'administrator', // Khả năng nhìn thấy menu của người dùng, trong ví dụ này chỉ người dùng với khả năng ‘manage_options’ có thể truy cập vào trang này
		'',
		'page_shortcodewoo',// ham de goi page khi click vao
		'dashicons-align-right'


	);
	

	add_action('admin_init','register_mysetting'); // hook de goi ham register_mysetting
}
add_action('admin_menu','addmenuwhortcode_woo'); // hook de goi ham addmenuwhortcode_woo

//======================= ham tao input ================
function page_shortcodewoo(){  
					
		// xu ly lay id da check ben ngoai gan vao value showid

		
	   		$argspostt = array(
	   			 'post_type' => 'product',
   			'meta_key' => 'some_meta_key',
   			'meta_value' => 'yes'
   		);
   		$all_post = new WP_Query( $argspostt );
   
   		$ids_post = '';
   		if( $all_post->have_posts() ) { 
   			while( $all_post->have_posts() ) { 
   				$all_post->the_post(); 
   				$ids_post .= get_the_id(). ',';

   			}
   		}
   		  $ids_post = rtrim($ids_post,",");

							
?>
<div class="wrap">
		<h1> Shortcode Settings WooCommerce</h1>
		<?php if( isset($_GET['settings-updated']) ) { ?>
			<div id="message" class="updated">
				<p><strong><?php _e('Settings saved.') ?></strong></p>
			</div>
		<?php } ?>
		<form method="post" action="options.php">
			<!-- dang ky setting fields de luu truong du lieu-->
			<?php 
			settings_fields('short-code-woo');
			?>

			<table class="form-table">
				<!-- cach hien thi-->
				<tr valign="top">
					<th scope="row">Display Show</th>
					<td>
						<select name="displayshow" class="form-control" id="id_displayshow">
							<option value="0">Please choise</option>
							<option value="showcategory">Display list Category</option>
							<option value="<?php echo $ids_post ?>">Display Id</option>
						</select>
						<td>
						</tr>

						<!-- hien thi theo the loai-->
						<tr valign="top" id="hide_category">
							<th scope="row">Display list Category</th>
							<td>
								<select name="fieldslistcategory" class="form-control" id="id_playcategory">
									<option value="0">Please choise</option>
									<?php

									$taxonomy     = 'product_cat';
									$orderby      = 'name';  
						  $show_count   = 0;      // 1 for yes, 0 for no
						  $pad_counts   = 0;      // 1 for yes, 0 for no
						  $hierarchical = 1;      // 1 for yes, 0 for no  
						  $title        = '';  
						  $empty        = 0;

						  $args = array(
						  	'taxonomy'     => $taxonomy,
						  	'orderby'      => $orderby,
						  	'show_count'   => $show_count,
						  	'pad_counts'   => $pad_counts,
						  	'hierarchical' => $hierarchical,
						  	'title_li'     => $title,
						  	'hide_empty'   => $empty
						  );
						  $all_categories = get_categories( $args );
						  foreach ($all_categories as $cat) {
						  	if($cat->category_parent == 0) {
						  		$category_id = $cat->term_id;     
						  		echo '<option value="'.$cat->slug.'">'.$cat->name.'</option>';  

						  	}       
						  }
						  ?>
						</select>
						<td>
						</tr>


						<!-- Sap sep theo ASC/DESC -->
						<tr valign="top">
							<th scope="row">Display Sorting by</th>
							<td>
								<select name="fieldsascdsc" class="form-control" id="id_fieldsascdsc">
									<option value="asc">ASC</option>
									<option value="desc">DESC</option>
								</select>
								<td>
								</tr>

								<!-- Hien thi theo dang luoi hay gird hay course -->
								<tr valign="top">
									<th scope="row">Option to show list</th>
									<td>
										<select name="showlist" class="form-control" id="id_showlist">
											<option value="list">List</option>
											<option value="grid">Grid</option>
											<option value="carousel">Carousel</option>
										</select>
										<td>
										</tr>
									</table>
									<input type="button" name="submit" id="submit" class="button button-primary" value="Shortcode">
								</form>
							</div>
							<div id="shortcodelsss">
							</div>
<?}

//=========================== check post external==========

// them 1 colum moi cho produce
add_filter( 'manage_edit-product_columns', 'misha_extra_column', 20 );
function misha_extra_column( $columns_array ) {
	$columns_array['extra'] = 'Checbox';
	
	return $columns_array;
 
}
 
// hanh dong chuc nang checbox
add_action( 'manage_posts_custom_column', 'misha_populate_columns' );
function misha_populate_columns( $column_name ) {
 
	if( $column_name  == 'extra' ) {
		echo '<input type="checkbox" data-productid="' . get_the_ID() .'" class="some_checkbox" ' . checked( 'yes', get_post_meta( get_the_ID(), 'some_meta_key', true ), false ) . '/><small style="display:block;color:#7ad03a"></small>';
	}
 
}
 
// hanh dong ajax luu gia tri 
add_action( 'admin_footer', 'misha_jquery_event' );
function misha_jquery_event(){
 
	echo "<script>jQuery(function($){
		$('.some_checkbox').click(function(){
			var checkbox = $(this),
			    checkbox_value = (checkbox.is(':checked') ? 'yes' : 'no' );
			$.ajax({
				type: 'POST',
				data: {
					action: 'productmetasave', // wp_ajax_{action} WordPress hook to process AJAX requests
					value: checkbox_value,
					product_id: checkbox.attr('data-productid'),
					myajaxnonce : '" . wp_create_nonce( "activatingcheckbox" ) . "'
				},
				beforeSend: function( xhr ) {
					checkbox.prop('disabled', true );
				},
				url: ajaxurl, // as usual, it is already predefined in /wp-admin
				success: function(data){
					checkbox.prop('disabled', false ).next().html(data).show().fadeOut(400);
				}
			});
		});
	});</script>";
 
}
 
// action luu gia tri
add_action( 'wp_ajax_productmetasave', 'misha_process_ajax' );
function misha_process_ajax(){
 
	check_ajax_referer( 'activatingcheckbox', 'myajaxnonce' );
 
	if( update_post_meta( $_POST[ 'product_id'] , 'some_meta_key', $_POST['value'] ) ) {
		echo 'Saved';
	}
 
	die();
}

//shortcode
add_shortcode('woocommercode','funshortcode');
function funshortcode($tr)
{
	ob_start();
		global $product; 

	 $idProduce =$tr['idproduce'];//if
	 $categoiid = $tr['category'];//if
	 $sortingby = $tr['sortingby'];
	 $display = $tr['display'];
    
    if($idProduce)
    {
    	if($sortingby=='asc'){ // if ma gia tri hien thi bang asc
    	$argspostt = array(
	   			 'post_type' => 'product',
	   			 'order' => 'asc',
	   			'meta_key' => 'some_meta_key',
	   			'meta_value' => 'yes'
   		);
      }
      if($sortingby=='desc'){// if ma gia tri hien thi bang desc
    	$argspostt = array(
	   			 'post_type' => 'product',
	   			 'order' => 'desc',
				'meta_key' => 'some_meta_key',
				'meta_value' => 'yes'
   		);
      }

   		$all_post = new WP_Query( $argspostt );

   		if( $all_post->have_posts() ) { ?>
   			   				<!-- if dislay list-->
   			   	<?php if($display=='grid'){ ?> <!-- neu co grid thi hien thi conatiner-->
   				<div class="container">
			     	<div class="row">
			     	<?php }?>
			     	 	<?php if($display=='carousel'){ ?> <!-- neu co carousel thi hien thi slide-->
   				<section id="demos">
   						<div class="row">
   							<div class="large-12 columns">
   								<div class="owl-carousel owl-theme">
			     	<?php }?>
   			<?php
   			while( $all_post->have_posts() ) { 
   				   		
   				$all_post->the_post(); 
                           $idget = get_the_id();
                          $_product = wc_get_product( $idget );
   				?>

   				<?php if($display=='list'){
   				 ?>
   		

				<li class="product">
				<a href="<?php echo get_the_guid(); ?>">
				<div class="product-images">
				<span class="onsale">Sale!</span>
				<span class="">
				<img style="height:300px;" width="300" height="300" src="<?php echo get_the_post_thumbnail_url() ?>" alt="" srcset="">
				</span>
				</div>
				<h2 class="woocommerce-loop-product__title"><?php the_title()?></h2>
				<del>
				<span class="woocommerce-Price-amount amount">
				<span class="woocommerce-Price-currencySymbol">$</span>
				<?php echo  $_product->get_variation_regular_price( 'max', true ); ?>
			   </span>
				</del> 
				<ins>
				<span class="woocommerce-Price-amount amount">
				<span class="woocommerce-Price-currencySymbol">$</span>
				<?php echo  $_product->get_variation_price( 'max', true ); ?>
				</span>
				</ins>
				</a>
				<div class="product-buttons">
				<div class="product-buttons-container clearfix">
				<a href="<?php echo get_the_guid(); ?>" data-quantity="1" class="button product_type_variable add_to_cart_button" rel="nofollow">Detail</a>
				</div>
				</li>

			     
   				<?php } ?><!-- if display list-->
   				<!-- if display grid-->			
   				<?php if($display=='grid'){ ?>

			     		<div class="col-md-3">
			            <li class="product">
						<a href="<?php echo get_the_guid(); ?>">
						<div class="product-images">
						<span class="onsale">Sale!</span>
						<span class="">
						<img style="height:300px;" width="300" height="300" src="<?php echo get_the_post_thumbnail_url() ?>" alt="" srcset="">
						</span>
						</div>
						<h2 class="woocommerce-loop-product__title"><?php the_title()?></h2>
						<del>
						<span class="woocommerce-Price-amount amount">
						<span class="woocommerce-Price-currencySymbol">$</span>
						<?php echo  $_product->get_variation_regular_price( 'max', true ); ?>
					   </span>
						</del> 
						<ins>
						<span class="woocommerce-Price-amount amount">
						<span class="woocommerce-Price-currencySymbol">$</span>
						<?php echo  $_product->get_variation_price( 'max', true ); ?>
						</span>
						</ins>
						</a>
						<div class="product-buttons">
						<div class="product-buttons-container clearfix">
						<a href="<?php echo get_the_guid(); ?>" data-quantity="1" class="button product_type_variable add_to_cart_button" rel="nofollow">Detail</a>
						</div>
						</li>
			     		</div>


   				<?php } ?><!-- if display grid-->

   					<!-- if display grid-->			
   				<?php if($display=='carousel'){ ?>

			     		<div class="item">
			           <li class="product">
							<a href="<?php echo get_the_guid(); ?>">
							<div class="product-images">
							<span class="onsale">Sale!</span>
							<span class="">
							<img style="height:300px;" width="300" height="300" src="<?php echo get_the_post_thumbnail_url() ?>" alt="" srcset="">
							</span>
							</div>
							<h2 class="woocommerce-loop-product__title"><?php the_title()?></h2>
							<del>
							<span class="woocommerce-Price-amount amount">
							<span class="woocommerce-Price-currencySymbol">$</span>
							<?php echo  $_product->get_variation_regular_price( 'max', true ); ?>
						   </span>
							</del> 
							<ins>
							<span class="woocommerce-Price-amount amount">
							<span class="woocommerce-Price-currencySymbol">$</span>
							<?php echo  $_product->get_variation_price( 'max', true ); ?>
							</span>
							</ins>
							</a>
							<div class="product-buttons">
							<div class="product-buttons-container clearfix">
							<a href="<?php echo get_the_guid(); ?>" data-quantity="1" class="button product_type_variable add_to_cart_button" rel="nofollow">Detail</a>
							</div>
							</li>
			     		</div>
   				<?php } ?><!-- if display grid-->


   			<?php } //while have_posts?>
   			<!-- neu co gid-->
   			<?php if($display=='grid'){ ?> 
			     	</div>
			     </div><!-- container-->
			 <?php }?>
			 <!-- neu co carousel thi hien thi div  silde-->
			 <?php if($display=='carousel'){ ?> <!-- neu co carousel thi hien thi slide-->
   				</div>
					<script>
						$(document).ready(function() {
							var owl = $('.owl-carousel');
							owl.owlCarousel({
								items: 3,
								loop: true,
								margin: 10,
								autoplay: true,
								autoplayTimeout: 1000,
								autoplayHoverPause: true
							});
							$('.play').on('click', function() {
								owl.trigger('play.owl.autoplay', [1000])
							})
							$('.stop').on('click', function() {
								owl.trigger('stop.owl.autoplay')
							})
						})
					</script>
				</div>
			</div>
		</section>
			     	<?php }?>

   		<?php } // if have_posts
    } // if idProduce

// if co category id
 if($categoiid)
    {
    	if($sortingby=='asc'){ // if ma gia tri hien thi bang asc
    	$argspostt = array(
    		'post_type' => 'product',
    		'product_cat' =>$categoiid,
	   			 'order' => 'asc',
   		);
      }
      if($sortingby=='desc'){// if ma gia tri hien thi bang desc
    	$argspostt = array(
    		'post_type' => 'product',
    		'product_cat' =>$categoiid,
	   			 'order' => 'desc',
   		);
      }

   		$all_post = new WP_Query( $argspostt );
   		if( $all_post->have_posts() ) { ?>
   			   				<!-- if dislay list-->
   			   	<?php if($display=='grid'){ ?> <!-- neu co grid thi hien thi conatiner-->
   				<div class="container">
			     	<div class="row">
			     	<?php }?>
			     	 	<?php if($display=='carousel'){ ?> <!-- neu co carousel thi hien thi slide-->
   				<section id="demos">
   						<div class="row">
   							<div class="large-12 columns">
   								<div class="owl-carousel owl-theme">
			     	<?php }?>
   			<?php
   			while( $all_post->have_posts() ) { 
   				$all_post->the_post();
   				           $idget = get_the_id();
                          $_product = wc_get_product( $idget );
                           ?>

   				<?php if($display=='list'){ ?>

			          <li class="product">
						<a href="<?php echo get_the_guid(); ?>">
						<div class="product-images">
						<span class="onsale">Sale!</span>
						<span class="">
						<img style="height:300px;" width="300" height="300" src="<?php echo get_the_post_thumbnail_url() ?>" alt="" srcset="">
						</span>
						</div>
						<h2 class="woocommerce-loop-product__title"><?php the_title()?></h2>
						<del>
						<span class="woocommerce-Price-amount amount">
						<span class="woocommerce-Price-currencySymbol">$</span>
						<?php echo  $_product->get_variation_regular_price( 'max', true ); ?>
					   </span>
						</del> 
						<ins>
						<span class="woocommerce-Price-amount amount">
						<span class="woocommerce-Price-currencySymbol">$</span>
						<?php echo  $_product->get_variation_price( 'max', true ); ?>
						</span>
						</ins>
						</a>
						<div class="product-buttons">
						<div class="product-buttons-container clearfix">
						<a href="<?php echo get_the_guid(); ?>" data-quantity="1" class="button product_type_variable add_to_cart_button" rel="nofollow">Detail</a>
						</div>
						</li>

			     
   				<?php } ?><!-- if display list-->
   				<!-- if display grid-->			
   				<?php if($display=='grid'){ ?>

			     		<div class="col-md-3"><li class="product">
							<a href="<?php echo get_the_guid(); ?>">
							<div class="product-images">
							<span class="onsale">Sale!</span>
							<span class="">
							<img style="height:300px;" width="300" height="300" src="<?php echo get_the_post_thumbnail_url() ?>" alt="" srcset="">
							</span>
							</div>
							<h2 class="woocommerce-loop-product__title"><?php the_title()?></h2>
							<del>
							<span class="woocommerce-Price-amount amount">
							<span class="woocommerce-Price-currencySymbol">$</span>
							<?php echo  $_product->get_variation_regular_price( 'max', true ); ?>
						   </span>
							</del> 
							<ins>
							<span class="woocommerce-Price-amount amount">
							<span class="woocommerce-Price-currencySymbol">$</span>
							<?php echo  $_product->get_variation_price( 'max', true ); ?>
							</span>
							</ins>
							</a>
							<div class="product-buttons">
							<div class="product-buttons-container clearfix">
							<a href="<?php echo get_the_guid(); ?>" data-quantity="1" class="button product_type_variable add_to_cart_button" rel="nofollow">Detail</a>
							</div>
							</li>
						</div>


   				<?php } ?><!-- if display grid-->

   					<!-- if display grid-->			
   				<?php if($display=='carousel'){ ?>

			     		<div class="item">
			          <li class="product">
						<a href="<?php echo get_the_guid(); ?>">
						<div class="product-images">
						<span class="onsale">Sale!</span>
						<span class="">
						<img style="height:300px;" width="300" height="300" src="<?php echo get_the_post_thumbnail_url() ?>" alt="" srcset="">
						</span>
						</div>
						<h2 class="woocommerce-loop-product__title"><?php the_title()?></h2>
						<del>
						<span class="woocommerce-Price-amount amount">
						<span class="woocommerce-Price-currencySymbol">$</span>
						<?php echo  $_product->get_variation_regular_price( 'max', true ); ?>
					   </span>
						</del> 
						<ins>
						<span class="woocommerce-Price-amount amount">
						<span class="woocommerce-Price-currencySymbol">$</span>
						<?php echo  $_product->get_variation_price( 'max', true ); ?>
						</span>
						</ins>
						</a>
						<div class="product-buttons">
						<div class="product-buttons-container clearfix">
						<a href="<?php echo get_the_guid(); ?>" data-quantity="1" class="button product_type_variable add_to_cart_button" rel="nofollow">Detail</a>
						</div>
						</li>
			     		</div>
   				<?php } ?><!-- if display grid-->


   			<?php } //while have_posts?>
   			<!-- neu co gid-->
   			<?php if($display=='grid'){ ?> 
			     	</div>
			     </div><!-- container-->
			 <?php }?>
			 <!-- neu co carousel thi hien thi div  silde-->
			 <?php if($display=='carousel'){ ?> <!-- neu co carousel thi hien thi slide-->
   				</div>
					<script>
						$(document).ready(function() {
							var owl = $('.owl-carousel');
							owl.owlCarousel({
								items: 3,
								loop: true,
								margin: 10,
								autoplay: true,
								autoplayTimeout: 1000,
								autoplayHoverPause: true
							});
							$('.play').on('click', function() {
								owl.trigger('play.owl.autoplay', [1000])
							})
							$('.stop').on('click', function() {
								owl.trigger('stop.owl.autoplay')
							})
						})
					</script>
				</div>
			</div>
		</section>
			     	<?php }?>

   		<?php } // if have_posts
    } // if idProduce
    //end category
	 //ket thuc ob_start
wp_reset_query();
return ob_get_clean(); 
}// if function funshortcode
