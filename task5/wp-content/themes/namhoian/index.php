<?php get_header(); ?>
  <!--==========================
    Header
    ============================-->
    <header id="header">
      <div class="container-fluid">

        <div id="logo" class="pull-left">
          <h1><a href="#intro" class="scrollto">WebNamHoiAn</a></h1>
          <!-- Uncomment below if you prefer to use an image logo -->
          <!-- <a href="#intro"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo.png" alt="" title="" /></a>-->
        </div>

        <nav id="nav-menu-container">
          <ul class="nav-menu">
            <?php 
            $menuLocations = get_nav_menu_locations(); 
            $menuID = $menuLocations['primary-menu'];
            $primaryNav = wp_get_nav_menu_items($menuID);
            ?>
            <?php 
            foreach ($primaryNav as $key => $value) { ?>
            <li ><a href="<?php echo $value->url ?>"><?php echo $value->title ?></a></li>
            <?php    }
            ?>

           <!--  <li><a class="menu-active" href="#about">About Us</a></li>
            <li><a href="#services">Services</a></li>
            <li><a href="#portfolio">Portfolio</a></li>
            <li><a href="#team">Team</a></li>
            <li class="menu-has-children"><a href="">Drop Down</a>
              <ul>
                <li><a href="#">Drop Down 1</a></li>
                <li><a href="#">Drop Down 3</a></li>
                <li><a href="#">Drop Down 4</a></li>
                <li><a href="#">Drop Down 5</a></li>
              </ul>
            </li>
            <li><a href="#contact">Contact</a></li> -->
          </ul>
        </nav><!-- #nav-menu-container -->
      </div>
    </header><!-- #header -->

  <!--==========================
    Intro Section
    ============================-->
    <section id="intro">
      <div class="intro-container">
        <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">

          <ol class="carousel-indicators"></ol>

          <div class="carousel-inner" role="listbox">
            <?php
            $args = array(
              'post_type' => 'banner_slide',
            );
            $the_query = new WP_Query( $args );
            if ( $the_query->have_posts() ) :
             $i=0;
             while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
             <?php// echo get_the_post_thumbnail_url(); ?>
             <div class="carousel-item 
             <?php 
             if($i==1)
             {
              echo get_post_meta('23','class_active_slide',true);
            }
            else{
              echo "fuckt";
            }
            ?>">
            <div class="carousel-background"><img src="<?php echo get_the_post_thumbnail_url(); ?>" alt=""></div>
            <div class="carousel-container">
              <div class="carousel-content">
                <h2><?php the_title(); ?> </h2>
                <p><?php the_content(); ?></p>
                <a href="#featured-services" class="btn-get-started scrollto">Xem tiếp</a>
              </div>
            </div>
          </div>
          <?php $i++; endwhile;
        endif;
        ?>

      </div>

      <a class="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>

      <a class="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>

    </div>
  </div>
</section><!-- #intro -->

<main id="main">

    <!--==========================
      Featured Services Section
      ============================-->
      <section id="featured-services">
        <div class="container">
          <div class="row">

            <div class="col-lg-4 box">
              <i class="ion-ios-bookmarks-outline"></i>
              <h4 class="title"><a href=""><?php $mypost = get_post(40);
              echo apply_filters('the_title',$mypost->post_title); ?></a></h4>
              <p class="description"><?php $mypost = get_post(40);
              echo apply_filters('the_content',$mypost->post_content); ?></p>
            </div>

            <div class="col-lg-4 box box-bg">
              <i class="ion-ios-stopwatch-outline"></i>
              <h4 class="title"><a href=""><?php $mypost = get_post(39);
              echo apply_filters('the_title',$mypost->post_title); ?></a></h4>
              <p class="description"><?php $mypost = get_post(39);
              echo apply_filters('the_content',$mypost->post_content); ?></p>
            </div>

            <div class="col-lg-4 box">
              <i class="ion-ios-heart-outline"></i>
              <h4 class="title"><a href=""><?php $mypost = get_post(42);
              echo apply_filters('the_title',$mypost->post_title); ?></a></h4>
              <p class="description"><?php $mypost = get_post(42);
              echo apply_filters('the_content',$mypost->post_content); ?></p>
            </div>

          </div>
        </div>
      </section><!-- #featured-services -->

    <!--==========================
      About Us Section
      ============================-->
      <section id="about">
        <div class="container">

          <header class="section-header">
            <h3>Giới Thiệu</h3>
            <!-- lấy giá trị cụ thể trong custom pot type theo id-->
            <p><?php $mypost = get_post(15);
            echo apply_filters('the_content',$mypost->post_content); ?></p>
          </header>

          <div class="row about-cols">
            <?php
            $my_post = get_post(29);
            $title = apply_filters('the_title',$my_post->post_title);
            $content = apply_filters('the_content',$my_post->post_content);
            $img_po = get_the_post_thumbnail_url($my_post);
            ?>
            <div class="col-md-4 wow fadeInUp">
              <div class="about-col">
                <div class="img">
                  <img style="height:205px;width:100%;" src="<?php echo $img_po; ?>" alt="" class="img-fluid">
                  <div class="icon"><i class="ion-ios-speedometer-outline"></i></div>
                </div>
                <h2 class="title"><a href="#"><?=$title?></a></h2>
                <p>
                  <?=$content?>
                </p>
              </div>
            </div>
            
            <?php
            $my_post1 = get_post(31);
            $title1 = apply_filters('the_title',$my_post1->post_title);
            $content1 = apply_filters('the_content',$my_post1->post_content);
            $img_po1 = get_the_post_thumbnail_url($my_post1);
            ?>
            <div class="col-md-4 wow fadeInUp" data-wow-delay="0.1s">
              <div class="about-col">
                <div class="img">
                  <img style="height:205px;width:100%;" src="<?php echo $img_po1;?>" alt="" class="img-fluid">
                  <div class="icon"><i class="ion-ios-list-outline"></i></div>
                </div>
                <h2 class="title"><a href="#"><?=$title1?></a></h2>
                <p>
                  <?=$content1?>
                </p>
              </div>
            </div>

            <?php
            $my_post2 = get_post(32);
            $title2 = apply_filters('the_title',$my_post2->post_title);
            $content2 = apply_filters('the_content',$my_post2->post_content);
            $img_po2 = get_the_post_thumbnail_url($my_post2);
            ?>
            <div class="col-md-4 wow fadeInUp" data-wow-delay="0.2s">
              <div class="about-col">
                <div class="img">
                  <img style="height:205px;width:100%;" src="<?php echo $img_po2; ?>" alt="" class="img-fluid">
                  <div class="icon"><i class="ion-ios-eye-outline"></i></div>
                </div>
                <h2 class="title"><a href="#"><?=$title2?></a></h2>
                <p>
                  <?=$content2?>
                </p>
              </div>
            </div>

          </div>

        </div>
      </section><!-- #about -->

    <!--==========================
      Services Section
      ============================-->
      <section id="services">
        <div class="container">

          <header class="section-header wow fadeInUp">
            <h3>Dịch Vụ</h3>
            <p><?php $mypost = get_post(16);
            echo apply_filters('the_content',$mypost->post_content); ?></p>
          </header>

          <div class="row">

            <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
              <div class="icon"><i class="ion-ios-analytics-outline"></i></div>
              <h4 class="title"><a href=""><?php $mypost = get_post(33);
              echo apply_filters('the_title',$mypost->post_title); ?>
              
            </a></h4>
            <p class="description"><?php $mypost = get_post(33);
            echo apply_filters('the_content',$mypost->post_content); ?></p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-bookmarks-outline"></i></div>
            <h4 class="title"><a href=""><?php $mypost = get_post(34);
            echo apply_filters('the_title',$mypost->post_title); ?></a></h4>
            <p class="description"><?php $mypost = get_post(34);
            echo apply_filters('the_content',$mypost->post_content); ?></p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-paper-outline"></i></div>
            <h4 class="title"><a href=""><?php $mypost = get_post(35);
            echo apply_filters('the_title',$mypost->post_title); ?></a></h4>
            <p class="description"><?php $mypost = get_post(35);
            echo apply_filters('the_content',$mypost->post_content); ?></p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-speedometer-outline"></i></div>
            <h4 class="title"><a href=""><?php $mypost = get_post(36);
            echo apply_filters('the_title',$mypost->post_title); ?></a></h4>
            <p class="description"><?php $mypost = get_post(36);
            echo apply_filters('the_content',$mypost->post_content); ?></p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-barcode-outline"></i></div>
            <h4 class="title"><a href=""><?php $mypost = get_post(37);
            echo apply_filters('the_title',$mypost->post_title); ?></a></h4>
            <p class="description"><?php $mypost = get_post(37);
            echo apply_filters('the_content',$mypost->post_content); ?></p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-delay="0.1s" data-wow-duration="1.4s">
            <div class="icon"><i class="ion-ios-people-outline"></i></div>
            <h4 class="title"><a href=""><?php $mypost = get_post(38);
            echo apply_filters('the_title',$mypost->post_title); ?></a></h4>
            <p class="description"><?php $mypost = get_post(38);
            echo apply_filters('the_content',$mypost->post_content); ?></p>
          </div>

        </div>

      </div>
    </section><!-- #services -->

    <!--==========================
      Call To Action Section
      ============================-->
      <section id="call-to-action" class="wow fadeIn">
        <div class="container text-center">


          <header class="section-header">
            <h3>Số liệu</h3>
            <p><?php $mypost = get_post(17);
            echo apply_filters('the_content',$mypost->post_content); ?></p>
          </header>

          <div class="row counters">

            <div class="col-lg-3 col-6 text-center">
              <span data-toggle="counter-up">200</span>
              <p>Khách hàng</p>
            </div>

            <div class="col-lg-3 col-6 text-center">
              <span data-toggle="counter-up">200</span>
              <p>Dự án</p>
            </div>

            <div class="col-lg-3 col-6 text-center">
              <span data-toggle="counter-up">20</span>
              <p>Đối tác</p>
            </div>

            <div class="col-lg-3 col-6 text-center">
              <span data-toggle="counter-up">1,400</span>
              <p>Lược truy cập</p>
            </div>

          </div>


        </div>
      </section><!-- #call-to-action -->

    <!--==========================
      Skills Section
      ============================-->
<!--     <section id="skills">
      <div class="container">

        <header class="section-header">
          <h3>Our Skills</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
        </header>

        <div class="skills-content">

          <div class="progress">
            <div class="progress-bar bg-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">HTML <i class="val">100%</i></span>
            </div>
          </div>

          <div class="progress">
            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">CSS <i class="val">90%</i></span>
            </div>
          </div>

          <div class="progress">
            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">JavaScript <i class="val">75%</i></span>
            </div>
          </div>

          <div class="progress">
            <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">Photoshop <i class="val">55%</i></span>
            </div>
          </div>

        </div>

      </div>
    </section> -->

    <!--==========================
      Portfolio Section
      ============================-->
      <section id="portfolio"  class="section-bg" >
        <div class="container">

          <header class="section-header">
            <h3 class="section-title">Dự Án</h3>
          </header>

          <div class="row">
            <div class="col-lg-12">
              <ul id="portfolio-flters">
                <li data-filter="*" class="filter-active">Tất cả</li>
                <li data-filter=".filter-app">Nhà hàng</li>
                <li data-filter=".filter-card">Shop</li>
                <li data-filter=".filter-web">Khác</li>
              </ul>
            </div>
          </div>

          <div class="row portfolio-container">

            <div class="col-lg-4 col-md-6 portfolio-item filter-app wow fadeInUp">
              <div class="portfolio-wrap">
                <figure>
                  <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/portfolio/app1.jpg" class="img-fluid" alt="">
                  <a href="<?php echo esc_url( get_template_directory_uri() ); ?>/img/portfolio/app1.jpg" data-lightbox="portfolio" data-title="App 1" class="link-preview" title="Preview"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </figure>

                <div class="portfolio-info">
                  <h4><a href="#">App 1</a></h4>
                  <p>App</p>
                </div>
              </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-web wow fadeInUp" data-wow-delay="0.1s">
              <div class="portfolio-wrap">
                <figure>
                  <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/portfolio/web3.jpg" class="img-fluid" alt="">
                  <a href="img/portfolio/web3.jpg" class="link-preview" data-lightbox="portfolio" data-title="Web 3" title="Preview"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </figure>

                <div class="portfolio-info">
                  <h4><a href="#">Web 3</a></h4>
                  <p>Web</p>
                </div>
              </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-app wow fadeInUp" data-wow-delay="0.2s">
              <div class="portfolio-wrap">
                <figure>
                  <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/portfolio/app2.jpg" class="img-fluid" alt="">
                  <a href="img/portfolio/app2.jpg" class="link-preview" data-lightbox="portfolio" data-title="App 2" title="Preview"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </figure>

                <div class="portfolio-info">
                  <h4><a href="#">App 2</a></h4>
                  <p>App</p>
                </div>
              </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-card wow fadeInUp">
              <div class="portfolio-wrap">
                <figure>
                  <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/portfolio/card2.jpg" class="img-fluid" alt="">
                  <a href="img/portfolio/card2.jpg" class="link-preview" data-lightbox="portfolio" data-title="Card 2" title="Preview"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </figure>

                <div class="portfolio-info">
                  <h4><a href="#">Card 2</a></h4>
                  <p>Card</p>
                </div>
              </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-web wow fadeInUp" data-wow-delay="0.1s">
              <div class="portfolio-wrap">
                <figure>
                  <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/portfolio/web2.jpg" class="img-fluid" alt="">
                  <a href="img/portfolio/web2.jpg" class="link-preview" data-lightbox="portfolio" data-title="Web 2" title="Preview"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </figure>

                <div class="portfolio-info">
                  <h4><a href="#">Web 2</a></h4>
                  <p>Web</p>
                </div>
              </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-app wow fadeInUp" data-wow-delay="0.2s">
              <div class="portfolio-wrap">
                <figure>
                  <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/portfolio/app3.jpg" class="img-fluid" alt="">
                  <a href="img/portfolio/app3.jpg" class="link-preview" data-lightbox="portfolio" data-title="App 3" title="Preview"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </figure>

                <div class="portfolio-info">
                  <h4><a href="#">App 3</a></h4>
                  <p>App</p>
                </div>
              </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-card wow fadeInUp">
              <div class="portfolio-wrap">
                <figure>
                  <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/portfolio/card1.jpg" class="img-fluid" alt="">
                  <a href="img/portfolio/card1.jpg" class="link-preview" data-lightbox="portfolio" data-title="Card 1" title="Preview"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </figure>

                <div class="portfolio-info">
                  <h4><a href="#">Card 1</a></h4>
                  <p>Card</p>
                </div>
              </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-card wow fadeInUp" data-wow-delay="0.1s">
              <div class="portfolio-wrap">
                <figure>
                  <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/portfolio/card3.jpg" class="img-fluid" alt="">
                  <a href="img/portfolio/card3.jpg" class="link-preview" data-lightbox="portfolio" data-title="Card 3" title="Preview"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </figure>

                <div class="portfolio-info">
                  <h4><a href="#">Card 3</a></h4>
                  <p>Card</p>
                </div>
              </div>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-item filter-web wow fadeInUp" data-wow-delay="0.2s">
              <div class="portfolio-wrap">
                <figure>
                  <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/portfolio/web1.jpg" class="img-fluid" alt="">
                  <a href="img/portfolio/web1.jpg" class="link-preview" data-lightbox="portfolio" data-title="Web 1" title="Preview"><i class="ion ion-eye"></i></a>
                  <a href="#" class="link-details" title="More Details"><i class="ion ion-android-open"></i></a>
                </figure>

                <div class="portfolio-info">
                  <h4><a href="#">Web 1</a></h4>
                  <p>Web</p>
                </div>
              </div>
            </div>

          </div>

        </div>
      </section><!-- #portfolio -->

    <!--==========================
      Clients Section
      ============================-->
      <section id="clients" class="wow fadeInUp">
        <div class="container">

          <header class="section-header">
            <h3>Đối tác</h3>
          </header>

          <div class="owl-carousel clients-carousel">
            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/clients/client-1.png" alt="">
            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/clients/client-2.png" alt="">
            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/clients/client-3.png" alt="">
            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/clients/client-4.png" alt="">
            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/clients/client-5.png" alt="">
            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/clients/client-6.png" alt="">
            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/clients/client-7.png" alt="">
            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/clients/client-8.png" alt="">
          </div>

        </div>
      </section><!-- #clients -->



    <!--==========================
      Team Section
      ============================-->
      <section id="team">
        <div class="container">
          <div class="section-header wow fadeInUp">
            <h3>Thành viên</h3>
            <p><?php $mypost = get_post(18);
            echo apply_filters('the_content',$mypost->post_content); ?></p>
          </div>

          <div class="row">
            <?php
            $args = array(
              'post_type' => 'thanh_vien',
            );
            $the_query = new WP_Query( $args );
            if ( $the_query->have_posts() ) :
              while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
              <?php// echo get_the_post_thumbnail_url(); ?>
              <div class="col-lg-3 col-md-6 wow fadeInUp">
                <div class="member">
                  <img src="<?php echo get_the_post_thumbnail_url()?>" class="img-fluid" alt="">
                  <div class="member-info">
                    <div class="member-info-content">
                      <h4><?php the_title(); ?></h4>
                      <span><?php the_content(); ?></span>
                      <!-- <div class="social">
                        <a href=""><i class="fa fa-twitter"></i></a>
                        <a href=""><i class="fa fa-facebook"></i></a>
                        <a href=""><i class="fa fa-google-plus"></i></a>
                        <a href=""><i class="fa fa-linkedin"></i></a>
                      </div> -->
                    </div>
                  </div>
                </div>
              </div>
            <?php endwhile;
          endif;
          ?>
        </div>

      </div>
    </section><!-- #team -->

    <!--==========================
      Contact Section
      ============================-->
      <section id="contact" class="section-bg wow fadeInUp">
        <div class="container">

          <div class="section-header">
            <h3>Liên Hệ Với Chúng Tôi</h3>
            <p><?php $mypost = get_post(19);
            echo apply_filters('the_content',$mypost->post_content); ?></p>
          </div>

          <div class="row contact-info">

            <div class="col-md-4">
              <div class="contact-address">
                <i class="ion-ios-location-outline"></i>
                <h3>Địa chỉ</h3>
                <address>Hội an - Đà nẳng</address>
              </div>
            </div>

            <div class="col-md-4">
              <div class="contact-phone">
                <i class="ion-ios-telephone-outline"></i>
                <h3>Số Điện thoại</h3>
                <p><a href="tel:+155895548855">0167 537 8489</a></p>
              </div>
            </div>

            <div class="col-md-4">
              <div class="contact-email">
                <i class="ion-ios-email-outline"></i>
                <h3>Email</h3>
                <p><a href="mailto:info@example.com">webnamhoian@gmail.com</a></p>
              </div>
            </div>

          </div>

          <div class="form">
            <div id="sendmessage">Your message has been sent. Thank you!</div>
            <div id="errormessage"></div>
            <!-- <form action="" method="post" role="form" class="contactForm">
              <div class="form-row">
                <div class="form-group col-md-6">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Tên của bạn" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validation"></div>
                </div>
                <div class="form-group col-md-6">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                  <div class="validation"></div>
                </div>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Tiêu đề" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                <div class="validation"></div>
              </div>
              <div class="form-group">
                <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Nội dung"></textarea>
                <div class="validation"></div>
              </div>
              <div class="text-center"><button type="submit">Gửi</button></div>
            </form> -->
           <?php echo do_shortcode('[gravityform id="1" title="true" description="true"]');?>
           <?php //echo do_shortcode('[contact-form-7 id="47" title="Form liên hệ 1"]');?>
          </div>

        </div>
      </section><!-- #contact -->

    </main>

  <!--==========================
    Footer
    ============================-->
    <footer id="footer">
      <div class="footer-top">
        <div class="container">
          <div class="row">

            <div class="col-lg-3 col-md-6 footer-info">
              <h2>WebNamHoiAn</h2>
              <p><?php $mypost = get_post(20);
              echo apply_filters('the_content',$mypost->post_content); ?></p>
            </div>

            <div class="col-lg-3 col-md-6 footer-links">
              <h4>Menu</h4>
              <ul>
               <?php 
               $menuLocations = get_nav_menu_locations(); 
               $menuID = $menuLocations['primary-menu'];
               $primaryNav = wp_get_nav_menu_items($menuID);
               ?>
               <?php 
               foreach ($primaryNav as $key => $value) { ?>
               <li ><i class="ion-ios-arrow-right"></i> <a href="<?php echo $value->url ?>"><?php echo $value->title ?></a></li>
               <?php    }
               ?>

             </ul>
           </div>

           <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Liên Hệ</h4>
            <p>
              Cup cấp dịch vụ <br>
              Hội an, Đà nẳng<br>
              <br>
              <strong>Điện thoại:</strong> 0167 537 8489<br>
              <strong>Email:</strong> webnamhoian@gmail.com<br>
            </p>

            <div class="social-links">
              <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
              <a target="target" href="https://www.facebook.com/webnamhoian/" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
              <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
            </div>

          </div>

          <div class="col-lg-3 col-md-6 footer-newsletter">
            <h4>Nhận thông báo</h4>
            <p>Hãy nhập email nếu bạn muốn nhận thông báo khuyến mãi dịch vụ từ chúng tôi.</p>
            <form action="" method="post">
              <input type="email" name="email"><input type="submit"  value="Gửi">
            </form>
          </div>

        </div>
      </div>
    </div>
    <?php get_footer(); ?>