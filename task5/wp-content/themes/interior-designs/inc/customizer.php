<?php
/**
 * Interior Designs Theme Customizer
 *
 * @package Interior Designs
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function interior_designs_customize_register( $wp_customize ) {

	//add home page setting pannel
	$wp_customize->add_panel( 'interior_designs_panel_id', array(
	    'priority' => 10,
	    'capability' => 'edit_theme_options',
	    'theme_supports' => '',
	    'title' => __( 'Theme Settings', 'interior-designs' ),
	    'description' => __( 'Description of what this panel does.', 'interior-designs' )
	) );

	//Layouts
	$wp_customize->add_section( 'interior_designs_left_right', array(
    	'title'      => __( 'Theme Layout Settings', 'interior-designs' ),
		'priority'   => 30,
		'panel' => 'interior_designs_panel_id'
	) );

	// Add Settings and Controls for Layout
	$wp_customize->add_setting('interior_designs_theme_options',array(
	        'default' => __( 'Right Sidebar', 'interior-designs' ),
	        'sanitize_callback' => 'interior_designs_sanitize_choices'
	) );

	$wp_customize->add_control('interior_designs_theme_options',
	    array(
	        'type' => 'radio',
	        'label' => __( 'Do you want this section', 'interior-designs' ),
	        'section' => 'interior_designs_left_right',
	        'choices' => array(
	            'Left Sidebar' => __('Left Sidebar','interior-designs'),
	            'Right Sidebar' => __('Right Sidebar','interior-designs'),
	            'One Column' => __('One Column','interior-designs'),
	            'Three Columns' => __('Three Columns','interior-designs'),
	            'Four Columns' => __('Four Columns','interior-designs'),
	            'Grid Layout' => __('Grid Layout','interior-designs')
	        ),
	    )
    );

    //topbar
	$wp_customize->add_section('interior_designs_topbar',array(
		'title'	=> __('Top Header','interior-designs'),
		'description'	=> __('Add Header Content here','interior-designs'),
		'priority'	=> null,
		'panel' => 'interior_designs_panel_id',
	));

    $wp_customize->add_setting('interior_designs_text',array(
		'default'	=> '',
		'sanitize_callback'	=> 'sanitize_text_field'
	));	
	$wp_customize->add_control('interior_designs_text',array(
		'label'	=> __('Site Text','interior-designs'),
		'section'	=> 'interior_designs_topbar',
		'setting'	=> 'interior_designs_text',
		'type'	=> 'text'
	));

	$wp_customize->add_setting('interior_designs_facebook_url',array(
		'default'	=> '',
		'sanitize_callback'	=> 'esc_url_raw'
	));
	
	$wp_customize->add_control('interior_designs_facebook_url',array(
		'label'	=> __('Add Facebook link','interior-designs'),
		'section'	=> 'interior_designs_topbar',
		'setting'	=> 'interior_designs_facebook_url',
		'type'	=> 'url'
	));

	$wp_customize->add_setting('interior_designs_twitter_url',array(
		'default'	=> '',
		'sanitize_callback'	=> 'esc_url_raw'
	));
	
	$wp_customize->add_control('interior_designs_twitter_url',array(
		'label'	=> __('Add Twitter link','interior-designs'),
		'section'	=> 'interior_designs_topbar',
		'setting'	=> 'interior_designs_twitter_url',
		'type'	=> 'url'
	));

	$wp_customize->add_setting('interior_designs_google_url',array(
		'default'	=> '',
		'sanitize_callback'	=> 'esc_url_raw'
	));	
	$wp_customize->add_control('interior_designs_google_url',array(
		'label'	=> __('Add Instagram link','interior-designs'),
		'section'	=> 'interior_designs_topbar',
		'setting'	=> 'interior_designs_google_url',
		'type'	=> 'url'
	));

	$wp_customize->add_setting('interior_designs_linkdin_url',array(
		'default'	=> '',
		'sanitize_callback'	=> 'esc_url_raw'
	));	
	$wp_customize->add_control('interior_designs_linkdin_url',array(
		'label'	=> __('Add Linkdin link','interior-designs'),
		'section'	=> 'interior_designs_topbar',
		'setting'	=> 'interior_designs_linkdin_url',
		'type'	=> 'url'
	));

	$wp_customize->add_setting('interior_designs_youtube_url',array(
		'default'	=> '',
		'sanitize_callback'	=> 'esc_url_raw'
	));	
	$wp_customize->add_control('interior_designs_youtube_url',array(
		'label'	=> __('Add Youtube link','interior-designs'),
		'section'	=> 'interior_designs_topbar',
		'setting'	=> 'interior_designs_youtube_url',
		'type'		=> 'url'
	));

	//Header
	$wp_customize->add_section('interior_designs_header',array(
		'title'	=> __('Header','interior-designs'),
		'description'	=> __('Add Header Content here','interior-designs'),
		'priority'	=> null,
		'panel' => 'interior_designs_panel_id',
	));

    $wp_customize->add_setting('interior_designs_call_text',array(
		'default'	=> '',
		'sanitize_callback'	=> 'sanitize_text_field'
	));	
	$wp_customize->add_control('interior_designs_call_text',array(
		'label'	=> __('Call Text','interior-designs'),
		'section'	=> 'interior_designs_header',
		'setting'	=> 'interior_designs_call_text',
		'type'	=> 'text'
	));

	$wp_customize->add_setting('interior_designs_call',array(
		'default'	=> '',
		'sanitize_callback'	=> 'sanitize_text_field'
	));	
	$wp_customize->add_control('interior_designs_call',array(
		'label'	=> __('Call No.','interior-designs'),
		'section'	=> 'interior_designs_header',
		'setting'	=> 'interior_designs_call',
		'type'	=> 'text'
	));

	$wp_customize->add_setting('interior_designs_location_text',array(
		'default'	=> '',
		'sanitize_callback'	=> 'sanitize_text_field'
	));	
	$wp_customize->add_control('interior_designs_location_text',array(
		'label'	=> __('Street Details','interior-designs'),
		'section'	=> 'interior_designs_header',
		'setting'	=> 'interior_designs_location_text',
		'type'	=> 'text'
	));

	$wp_customize->add_setting('interior_designs_location',array(
		'default'	=> '',
		'sanitize_callback'	=> 'sanitize_text_field'
	));	
	$wp_customize->add_control('interior_designs_location',array(
		'label'	=> __('City Details','interior-designs'),
		'section'	=> 'interior_designs_header',
		'setting'	=> 'interior_designs_location',
		'type'	=> 'text'
	));

	$wp_customize->add_setting('interior_designs_time',array(
		'default'	=> '',
		'sanitize_callback'	=> 'sanitize_text_field'
	));	
	$wp_customize->add_control('interior_designs_time',array(
		'label'	=> __('Time','interior-designs'),
		'section'	=> 'interior_designs_header',
		'setting'	=> 'interior_designs_time',
		'type'	=> 'text'
	));

	$wp_customize->add_setting('interior_designs_day',array(
		'default'	=> '',
		'sanitize_callback'	=> 'sanitize_text_field'
	));	
	$wp_customize->add_control('interior_designs_day',array(
		'label'	=> __('Day','interior-designs'),
		'section'	=> 'interior_designs_header',
		'setting'	=> 'interior_designs_day',
		'type'	=> 'text'
	));

	//home page slider
	$wp_customize->add_section( 'interior_designs_slidersettings' , array(
    	'title'      => __( 'Slider Settings', 'interior-designs' ),
		'priority'   => null,
		'panel' => 'interior_designs_panel_id'
	) );

	for ( $count = 1; $count <= 4; $count++ ) {

		// Add color scheme setting and control.
		$wp_customize->add_setting( 'interior_designs_slider_page' . $count, array(
			'default'           => '',
			'sanitize_callback' => 'interior_designs_sanitize_dropdown_pages'
		) );

		$wp_customize->add_control( 'interior_designs_slider_page' . $count, array(
			'label'    => __( 'Select Slide Image Page', 'interior-designs' ),
			'section'  => 'interior_designs_slidersettings',
			'type'     => 'dropdown-pages'
		) );

	}

	//Discover More
	$wp_customize->add_section('interior_designs_discover',array(
		'title'	=> __('Discover Section','interior-designs'),
		'description'	=> __('Add About sections below.','interior-designs'),
		'panel' => 'interior_designs_panel_id',
	));

	$post_list = get_posts();
	$i = 0;
	foreach($post_list as $post){
		$posts[$post->post_title] = $post->post_title;
	}

	$wp_customize->add_setting('interior_designs_discover_post',array(
		'sanitize_callback' => 'interior_designs_sanitize_choices',
	));
	$wp_customize->add_control('interior_designs_discover_post',array(
		'type'    => 'select',
		'choices' => $posts,
		'label' => __('Select post','interior-designs'),
		'section' => 'interior_designs_discover',
	));


	//Services
	$wp_customize->add_section('interior_designs_services',array(
		'title'	=> __('Services Section','interior-designs'),
		'description'=> __('This section will appear below the slider.','interior-designs'),
		'panel' => 'interior_designs_panel_id',
	));	
	
	$wp_customize->add_setting('interior_designs_service_title',array(
		'default'=> '',
		'sanitize_callback'	=> 'sanitize_text_field'
	));	
	$wp_customize->add_control('interior_designs_service_title',array(
		'label'	=> __('Section Title','interior-designs'),
		'section'=> 'interior_designs_services',
		'setting'=> 'interior_designs_service_title',
		'type'=> 'text'
	));
	
	$wp_customize->add_setting('interior_designs_service_tag_line',array(
		'default'=> '',
		'sanitize_callback'	=> 'sanitize_text_field'
	));
	$wp_customize->add_control('interior_designs_service_tag_line',array(
		'label'	=> __('Section Sub-Title','interior-designs'),
		'section'=> 'interior_designs_services',
		'setting'=> 'interior_designs_service_tag_line',
		'type'=> 'text'
	));

	$categories = get_categories();
		$cats = array();
			$i = 0;
			foreach($categories as $category){
			if($i==0){
			$default = $category->slug;
			$i++;
		}
		$cats[$category->slug] = $category->name;
	}

	$wp_customize->add_setting('interior_designs_services_category',array(
		'default'	=> 'select',
		'sanitize_callback' => 'sanitize_text_field',
	));
	$wp_customize->add_control('interior_designs_services_category',array(
		'type'    => 'select',
		'choices' => $cats,
		'label' => __('Select Category to display Latest Post','interior-designs'),
		'section' => 'interior_designs_services',
	));

	//Footer
	$wp_customize->add_section('interior_designs_footer_section',array(
		'title'	=> __('Copyright','interior-designs'),
		'description'	=> '',
		'priority'	=> null,
		'panel' => 'interior_designs_panel_id',
	));
	
	$wp_customize->add_setting('interior_designs_footer_copy',array(
		'default'	=> '',
		'sanitize_callback'	=> 'sanitize_text_field',
	));	
	$wp_customize->add_control('interior_designs_footer_copy',array(
		'label'	=> __('Copyright Text','interior-designs'),
		'section'	=> 'interior_designs_footer_section',
		'type'		=> 'text'
	));
	/** home page setions end here**/	
}
add_action( 'customize_register', 'interior_designs_customize_register' );


/**
 * Singleton class for handling the theme's customizer integration.
 *
 * @since  1.0.0
 * @access public
 */
final class Interior_Designs_Customize {

	/**
	 * Returns the instance.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return object
	 */
	public static function get_instance() {

		static $instance = null;

		if ( is_null( $instance ) ) {
			$instance = new self;
			$instance->setup_actions();
		}

		return $instance;
	}

	/**
	 * Constructor method.
	 *
	 * @since  1.0.0
	 * @access private
	 * @return void
	 */
	private function __construct() {}

	/**
	 * Sets up initial actions.
	 *
	 * @since  1.0.0
	 * @access private
	 * @return void
	 */
	private function setup_actions() {

		// Register panels, sections, settings, controls, and partials.
		add_action( 'customize_register', array( $this, 'sections' ) );

		// Register scripts and styles for the controls.
		add_action( 'customize_controls_enqueue_scripts', array( $this, 'enqueue_control_scripts' ), 0 );
	}

	/**
	 * Sets up the customizer sections.
	 *
	 * @since  1.0.0
	 * @access public
	 * @param  object  $manager
	 * @return void
	 */
	public function sections( $manager ) {

		// Load custom sections.
		load_template( trailingslashit( get_template_directory() ) . '/inc/section-pro.php' );
		
		// Register custom section types.
		$manager->register_section_type( 'Interior_Designs_Customize_Section_Pro' );

		// Register sections.
		$manager->add_section(
			new Interior_Designs_Customize_Section_Pro(
				$manager,
				'example_1',
				array(
					'priority'   => 9,
					'title'    => esc_html__( 'Interior Pro Theme', 'interior-designs' ),
					'pro_text' => esc_html__( 'Go Pro',         'interior-designs' ),
					'pro_url'  => esc_url( 'https://www.themescaliber.com/themes/interior-design-wordpress-theme/' ),
				)
			)
		);
	}

	/**
	 * Loads theme customizer CSS.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function enqueue_control_scripts() {

		wp_enqueue_script( 'interior-designs-customize-controls', trailingslashit( get_template_directory_uri() ) . '/js/customize-controls.js', array( 'customize-controls' ) );

		wp_enqueue_style( 'interior-designs-customize-controls', trailingslashit( get_template_directory_uri() ) . '/css/customize-controls.css' );
	}
}

// Doing this customizer thang!
Interior_Designs_Customize::get_instance();