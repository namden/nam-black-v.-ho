<?php
function style()
{
	//============================= Css =============================================
	// <!-- Google Fonts -->
	wp_enqueue_style('google font','https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet');
	//Bootstrap
	wp_enqueue_style('bootstrap',get_template_directory_uri().'/lib/bootstrap/css/bootstrap.min.css');
	//Libraries CSS Files
	wp_enqueue_style('Libraries',get_template_directory_uri().'/lib/font-awesome/css/font-awesome.min.css');
	wp_enqueue_style('animates',get_template_directory_uri().'/lib/animate/animate.min.css');
	wp_enqueue_style('ionicons',get_template_directory_uri().'/lib/ionicons/css/ionicons.min.css');
	wp_enqueue_style('owlcarousel',get_template_directory_uri().'/lib/owlcarousel/assets/owl.carousel.min.css');
	wp_enqueue_style('lightbox',get_template_directory_uri().'/lib/lightbox/css/lightbox.min.css');
	wp_enqueue_style('stylecss',get_template_directory_uri().'/css/style.css');
	//================================ Script =========================================
	wp_enqueue_script('jquery',get_template_directory_uri('') .'/lib/jquery/jquery.min.js');
	wp_enqueue_script('jquerymigrate',get_template_directory_uri('') .'/lib/jquery/jquery-migrate.min.js');
	wp_enqueue_script('bootstrapbundle',get_template_directory_uri('') .'/lib/bootstrap/js/bootstrap.bundle.min.js');
	wp_enqueue_script('easing',get_template_directory_uri('') .'/lib/easing/easing.min.js');
	wp_enqueue_script('superfishhoverIntent',get_template_directory_uri('') .'/lib/superfish/hoverIntent.js');
	wp_enqueue_script('superfish',get_template_directory_uri('') .'/lib/superfish/superfish.min.js');
	wp_enqueue_script('wow',get_template_directory_uri('') .'/lib/wow/wow.min.js');
	wp_enqueue_script('waypoints',get_template_directory_uri('') .'/lib/waypoints/waypoints.min.js');
	wp_enqueue_script('counterup',get_template_directory_uri('') .'/lib/counterup/counterup.min.js');
	wp_enqueue_script('owlcarouselmin',get_template_directory_uri('') .'/lib/owlcarousel/owl.carousel.min.js');
	wp_enqueue_script('isotope',get_template_directory_uri('') .'/lib/isotope/isotope.pkgd.min.js');
	wp_enqueue_script('lightbox',get_template_directory_uri('') .'/lib/lightbox/js/lightbox.min.js');
	wp_enqueue_script('touchSwipe',get_template_directory_uri('') .'/lib/touchSwipe/jquery.touchSwipe.min.js');
	wp_enqueue_script('contactform',get_template_directory_uri('') .'/contactform/contactform.js');
	wp_enqueue_script('main',get_template_directory_uri('') .'/js/main.js');
}
add_action( 'wp_enqueue_scripts', 'style' );
//regitter menu
add_theme_support( 'menus' );

function register_theme_menus () {
  register_nav_menus( [
    'primary-menu' => _( 'Primary Menu' )
  ] );
}

add_action( 'init', 'register_theme_menus' );
// register img
add_theme_support( 'post-thumbnails' ); 