<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package ishop
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'ishop' ); ?></a>
  <?php if ( has_nav_menu( 'topmenu' ) ) { get_template_part('template-parts/topmenu'); }?>
	<header id="masthead" class="site-header" role="banner">

    <div class="header-area">
        
	 <div class="large-6 columns">
            <?php if ( ishop_the_custom_logo()  ) { ishop_the_custom_logo();  }
   else {
	   
	  if ( is_front_page() && is_home() ) : ?>
						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
						<?php $description = get_bloginfo( 'description', 'display' );
					if ( $description || is_customize_preview() ) : ?>
						<p class="site-description"><?php echo esc_html($description); ?></p>
						<?php endif; ?>
					<?php else : ?>
						<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
<?php $description = get_bloginfo( 'description', 'display' );
					if ( $description || is_customize_preview() ) : ?>
						<p class="site-description"><?php echo esc_html($description); ?></p>
						<?php endif; ?>
					<?php endif;
   }
   ?> 			
				
                 </div> 
            <div class="large-6 columns asidelogo">
                 <?php if (!dynamic_sidebar('topright') ) : endif; ?>              

            </div>
    </div>
		</header><!-- #masthead -->
		<?php if ( has_nav_menu( 'primary' ) ) { ?>
		<nav id="site-navigation" class="main-navigation" role="navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'ishop' ); ?></button>
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
		</nav><!-- #site-navigation -->    
		<?php } ?>
	
	<div id="content" class="site-content">
		<div class="large-12 columns belownavi">
			<?php
				if (!dynamic_sidebar('belownavi') ) : endif;
				get_template_part('template-parts/slider');
				if (get_theme_mod('hide_news_ticker')==''){
				echo ishop_ticker(); }
			?>
	</div>