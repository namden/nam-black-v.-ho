<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content-ma">
 *
 * @package Interior Designs
 */

?><!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width">
  <link rel="profile" href="<?php echo esc_url( __( 'http://gmpg.org/xfn/11', 'interior-designs' ) ); ?>">
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> class="main-bodybox">
  	<div class="top-header">
	  	<div class="container">
	    	<div class="row">
	      		<div class="col-md-7 col-sm-7">
	        		<div class="site-text">
	          			<?php if( get_theme_mod( 'interior_designs_text','' ) != '') { ?>
	            			<span class="phone"><?php echo esc_html( get_theme_mod('interior_designs_text',__('Welcome to Interior Designs Theme','interior-designs') )); ?></span>
	           			<?php } ?>
	        		</div>
	      		</div>
	      		<div class="col-md-5 col-sm-5">
	        		<div class="social-media">
	          			<?php if( get_theme_mod( 'interior_designs_facebook_url','' ) != '') { ?>
	            			<a href="<?php echo esc_url( get_theme_mod( 'interior_designs_facebook_url','' ) ); ?>"><i class="fab fa-facebook-f"></i></a>
	          			<?php } ?>
	          			<?php if( get_theme_mod( 'interior_designs_twitter_url','' ) != '') { ?>
	            			<a href="<?php echo esc_url( get_theme_mod( 'interior_designs_twitter_url','' ) ); ?>"><i class="fab fa-twitter"></i></a>
	          			<?php } ?>
	          			<?php if( get_theme_mod( 'interior_designs_google_url','' ) != '') { ?>
	            			<a href="<?php echo esc_url( get_theme_mod( 'interior_designs_google_url','' ) ); ?>"><i class="fab fa-google-plus-g"></i></a>
	          			<?php } ?>
	          			<?php if( get_theme_mod( 'interior_designs_linkdin_url','' ) != '') { ?>
	            			<a href="<?php echo esc_url( get_theme_mod( 'interior_designs_linkdin_url','' ) ); ?>"><i class="fab fa-linkedin-in"></i></a>
	          			<?php } ?>
	          			<?php if( get_theme_mod( 'interior_designs_youtube_url','' ) != '') { ?>
	            			<a href="<?php echo esc_url( get_theme_mod( 'interior_designs_youtube_url','' ) ); ?>"><i class="fab fa-youtube"></i></a>
	          			<?php } ?>
	        		</div>
	      		</div>
	      		<div class="clearfix"></div>
	    	</div>
	  	</div>
	</div>
  	<div class="site_header">
	  	<div class="container">
	  		<div class="row">
			    <div class="col-md-4 col-sm-4">
			    	<div class="logo">
				      	<?php if( has_custom_logo() ){ interior_designs_the_custom_logo();
				         	}else{ ?>
				        	<h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				        <?php $description = get_bloginfo( 'description', 'display' );
				        	if ( $description || is_customize_preview() ) : ?> 
				          <p class="site-description"><?php echo esc_html($description); ?></p>       
				      	<?php endif; }?>
				    </div>
			    </div>
			    <div class="col-md-8 col-sm-8 row">
			        <div class="call col-md-4 col-sm-4 row">
			          	<?php if( get_theme_mod( 'interior_designs_call','' ) != '') { ?>
			            	<div class="col-md-2"><i class="fas fa-phone"></i></div>
		            		<div class="col-md-10">
			             		<p class="infotext"><?php echo esc_html( get_theme_mod('interior_designs_call_text',__('Call Us','interior-designs')) ); ?></p>
			              		<p><?php echo esc_html( get_theme_mod('interior_designs_call',__('+91 968 923 9632','interior-designs')) ); ?></p>
			            	</div>
			          	<?php } ?>
			        </div>
			        <div class="location col-md-4 col-sm-4 row">
			         	<?php if( get_theme_mod( 'interior_designs_location','' ) != '') { ?>
				            <div class="col-md-2"><i class="fas fa-location-arrow"></i></div>
				            <div class="col-md-10">
				              	<p class="infotext"><?php echo esc_html( get_theme_mod('interior_designs_location_text',__('123 Dummy Street Name 123','interior-designs')) ); ?></p>
				              	<p><?php echo esc_html( get_theme_mod('interior_designs_location',__('New york City, USA','interior-designs')) ); ?></p>
				            </div>
			          	<?php } ?>
			        </div>
			        <div class="time col-md-4 col-sm-4 row">
			         	<?php if( get_theme_mod( 'interior_designs_day','' ) != '') { ?>
				            <div class="col-md-2"><i class="far fa-clock"></i></div>
				            <div class="col-md-10">
				              	<p class="infotext"><?php echo esc_html( get_theme_mod('interior_designs_time',__('8:00 AM - 6:00 PM','interior-designs')) ); ?></p>
				              	<p><?php echo esc_html( get_theme_mod('interior_designs_day',__('Monday to Saturday','interior-designs')) ); ?></p>
				            </div>
			          	<?php } ?>
			        </div>
		      	</div>
			</div>
	    </div>		
	</div>	
	<div class="container">
		<div class="toggle"><a class="toggleMenu" href="#"><?php esc_html_e('Menu','interior-designs'); ?></a></div>
		<div id="header" class="row">
			<div class="menubox nav col-md-11">
			    <div class="mainmenu">
			      <?php wp_nav_menu( array('theme_location'  => 'primary') ); ?>
			    </div>
				<div class="clear"></div>
			</div>
			<div class="search-box col-md-1">
				<span><i class="fas fa-search"></i></span>
			</div>
			<div class="serach_outer">
	  		  	<div class="closepop"><i class="far fa-window-close"></i></div>
	  			<div class="serach_inner">
	  				<?php get_search_form(); ?>
	  			</div>
	  	  	</div>
		</div>
	</div>