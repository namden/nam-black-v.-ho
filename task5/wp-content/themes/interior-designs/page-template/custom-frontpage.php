<?php
/**
 * The template for displaying home page.
 *
 * Template Name: Custom Home Page
 *
 * @package Interior Designs
 */

get_header(); ?>

<section id="slider">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel"> 
      <?php $pages = array();
        for ( $count = 1; $count <= 3; $count++ ) {
          $mod = intval( get_theme_mod( 'interior_designs_slider_page' . $count ));
          if ( 'page-none-selected' != $mod ) {
            $pages[] = $mod;
          }
        }
        if( !empty($pages) ) :
          $args = array(
            'post_type' => 'page',
            'post__in' => $pages,
            'orderby' => 'post__in'
          );
          $query = new WP_Query( $args );
          if ( $query->have_posts() ) :
            $i = 1;
      ?>     
      <div class="carousel-inner" role="listbox">
        <?php  while ( $query->have_posts() ) : $query->the_post(); ?>
          <div <?php if($i == 1){echo 'class="carousel-item active"';} else{ echo 'class="carousel-item"';}?>>
            <a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url('full'); ?>"/></a>           
          </div>
        <?php $i++; endwhile; 
        wp_reset_postdata();?>
      </div>
      <?php else : ?>
          <div class="no-postfound"></div>
        <?php endif;
      endif;?>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"><?php esc_html_e('Previous','interior-designs'); ?></span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"><?php esc_html_e('Next','interior-designs'); ?></span>
      </a>
    </div>
    
    <div class="clearfix"></div>
</section> 

<?php do_action( 'interior_designs_after_slider' ); ?>

<section id="discover">
  	<div class="container">
	  	<div class="row">
		    <?php
		      $args = array( 'name' => get_theme_mod('interior_designs_discover_post',''));
		      $query = new WP_Query( $args );
		      if ( $query->have_posts() ) :
		        while ( $query->have_posts() ) : $query->the_post(); ?>
		    		<div class="col-md-8">
			            <h3><?php the_title(); ?></h3>
			            <p><?php $excerpt = get_the_excerpt(); echo esc_html( interior_designs_string_limit_words( $excerpt,15 ) ); ?></p>
			        </div>
		            <div class ="disc-btn col-md-4">
		              	<a href="<?php the_permalink(); ?>"><?php echo esc_html_e('Discover More','interior-designs') ?></a>
		            </div>
		        <?php endwhile; 
		        wp_reset_postdata();?>
		        <?php else : ?>
		          <div class="no-postfound"></div>
		        <?php
		    endif; ?>
		</div>
	</div>
</section>

<?php do_action( 'interior_designs_after_service' ); ?>

<section id="services">
	<div class="container">
		<?php if( get_theme_mod('interior_designs_service_title') != ''){ ?>     
            <h3><?php echo esc_html(get_theme_mod('interior_designs_service_title',__('Services We Offered','interior-designs'))); ?></h3>
        <?php }?>
        <?php if( get_theme_mod('interior_designs_service_tag_line') != ''){ ?>     
            <p><?php echo esc_html(get_theme_mod('interior_designs_service_tag_line',__('Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s.','interior-designs'))); ?></p>
        <?php }?>
        <div class="row">
	        <?php 
	         	$page_query = new WP_Query(array( 'category_name' => esc_html(get_theme_mod('interior_designs_services_category'),'theblog')));?>
	          	<?php while( $page_query->have_posts() ) : $page_query->the_post(); ?>
	          		<div class="col-md-3">
		                <?php if(has_post_thumbnail()) { ?><?php the_post_thumbnail(); ?><?php } ?>
		                <a href="<?php the_permalink(); ?>"><h4><?php the_title(); ?></h4></a>
		            </div>
	          	<?php endwhile;
	          	wp_reset_postdata();
	        ?>
	    </div>
	</div>
</section>

<?php do_action( 'interior_designs_after_product' ); ?>

<div id="content-ma">
	<div class="container">
  	<?php while ( have_posts() ) : the_post(); ?>
      <?php the_content(); ?>
    <?php endwhile; // end of the loop. ?>
	</div>
</div>

<?php get_footer(); ?>