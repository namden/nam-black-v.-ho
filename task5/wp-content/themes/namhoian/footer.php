 <div class="container">
      <div class="copyright">
        &copy; Bản quyền <strong>Webnamhoian</strong>
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=BizPage
        -->
        Thiết kế bởi<a href="https://bootstrapmade.com/"></a><a href=""> Webnamhoian.com</a>
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
<?php wp_footer(); ?>
</body>
</html>
