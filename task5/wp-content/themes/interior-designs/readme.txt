/*----------------------------------------------------------------------------*/
/* Interior Designs WordPress Theme */
/*----------------------------------------------------------------------------*/

Theme Name      :   Interior Designs
Theme URI       :   https://www.themescaliber.com/themes/free-interior-design-wordpress-theme/
Version         :   0.4.2
Tested up to    :   WP 4.9.7
Author          :   ThemesCaliber
Author URI      :   https://www.themescaliber.com
license         :   GNU General Public License v3.0
License URI     :   http://www.gnu.org/licenses/gpl.html

/*----------------------------------------------------------------------------*/
/* About Author - Contact Details */
/*----------------------------------------------------------------------------*/

Email       	:   support@themescaliber.com

/*----------------------------------------------------------------------------*/
/* Features */
/*----------------------------------------------------------------------------*/

Manage Slider, services and footer from admin customizer theme setting section.

/*----------------------------------------------------------------------------*/
/* Home Page Setup Steps*/
/*----------------------------------------------------------------------------*/
Below are the steps to setup theme static page.
=========================================================
	Step 1. Create a page named as "home page" and select the template "Custom Home Page".
	Step 2. Go to customizer >> static front page >> check Static page, then select the page which you have added (for example "home page").

For Top Header
================
	Step 1. Go to customizer >> Theme Settings >> Top Header >> here you can add your text and social links.

For Header
============
	Step 1. Go to customizer >> Theme Settings >> Header >> here you can add your contact details.

For Slider
==============
	Step 1. Create a page, add its title, content and featured image then publish it.
	Step 2. Go to customizer >> Theme Settings >> Slider settings >> here you can select the page which you have added.

For Discover Section
=====================
	Step 1. Add new post, add post title, featured image and content then publish it
	Step 2. Go to customizer >> Theme Settings >> Discover Section >> here you can get the option to select the post.

For Services Section
=====================
	Step 1. Add new category.
	Step 2. Add new post, add post title, featured image, content and assign the category which you have added then publish it
	Step 3. Go to customizer >> Theme Settings >> Services Section >> here you can get the option to select the category and the title text.

/*----------------------------------------------------------------------------*/
/* Theme Resources */
/*----------------------------------------------------------------------------*/

Interior Designs WordPress Theme, Copyright 2018 ThemesCaliber
Interior Designs is distributed under the terms of the GNU GPL

Theme is Built using the following resource bundles.

1 - CSS bootstrap.css
    -- Copyright 2011-2018 The Bootstrap Authors
    -- https://github.com/twbs/bootstrap/blob/master/LICENSE
    
2 - JS bootstrap.js
    -- Copyright 2011-2018 The Bootstrap Authors (https://github.com/twbs/bootstrap/graphs/contributors)
    -- https://github.com/twbs/bootstrap/blob/master/LICENSE

3	Font License:font-awesome/fonts/.
	License: SIL OFL 1.1
	URL: http://scripts.sil.org/OFL

4	Code License: font-awesome/css/, font-awesome/less/, and font-awesome/scss/.
	License: MIT License
	URL: http://opensource.org/licenses/mit-license.html

5	Customizer Licence
	All code, unless otherwise noted, is licensed under the GNU GPL, version 2 or later.
	2016 © Justin Tadlock.

6   Pexel Images
    License: CC0 1.0 Universal (CC0 1.0)
    Source: https://www.pexels.com/photo-license/

	Header image, Copyright diegodiezperez123
	License: CC0 1.0 Universal (CC0 1.0)
	Source: https://www.pexels.com/photo/apartment-chair-clean-contemporary-279719/

	Services image, Copyright artursfoto
	License: CC0 1.0 Universal (CC0 1.0)
	Source: https://www.pexels.com/photo/apply-blue-brush-carpentry-221027/

	Services image, Copyright gdtography
	License: CC0 1.0 Universal (CC0 1.0)
	Source: https://www.pexels.com/photo/long-exposure-photography-white-dome-building-interior-911758/

	Services image, Copyright Scott Webb
	License: CC0 1.0 Universal (CC0 1.0)
	Source: https://www.pexels.com/photo/abstract-architectural-design-architecture-building-136419/

	Services image, Copyright ninabobo88 
	License: CC0 1.0 Universal (CC0 1.0)
	Source: https://www.pexels.com/photo/blur-bottle-bright-building-273238/
