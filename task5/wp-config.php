<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

define('FS_METHOD', 'direct');
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'task4moi');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_X~fUnt:^_X75BQ53E^!Pf6~Loj8j/Eg~/RMyEbm2qsD|q-A5;t,R(WT_<=)XvZ_');
define('SECURE_AUTH_KEY',  'ds&-64ZEi:-9)R! 2<pu;^8ET$(6*Z|kEM0!RrK-p>|[<WP3O*tJ ^^qZso*xTJ:');
define('LOGGED_IN_KEY',    ';v7NY9IMutji89pEnv0orNbrtLclR{t);0511gEJ~XI%QI.z@eYLWuCMTYJ!<V{G');
define('NONCE_KEY',        '&1zDWBV1CjlpHeElQ.0x4kj~<@tCfzgJi&.uyTC1sYRW9DxI%l*Q]0R-C(E/sP#%');
define('AUTH_SALT',        'gh8rBi@4OweD%!1^5J Lxn`:ZAlH#)8JA>F~HJeZq}<G+)/|253ftb`p+{Z:`/JA');
define('SECURE_AUTH_SALT', '(6h5K=-V?[bA@*Lytb_/XYwp9$yai;9pX$K;k3e}D##d*z|U>xW<5(+ST:n0|-A<');
define('LOGGED_IN_SALT',   ',oLD`ni]E4R<Y]aksS6?j#6*3p$6woqO7xV6~4;V] aXOU.dH@Ko;KN?*o:p}fA#');
define('NONCE_SALT',       ':Ec3kq}akQj>7!8FM<pG_W>#7TCmkYqoi)L)S3Z+8^;bGq=RX@HYSh(97w6Q0a(]');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
